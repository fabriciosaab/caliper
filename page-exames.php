<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            get_template_part( 'incs/partial/partial', 'box-title' );
            $args = array(
                'post_type'=> 'exams',
                'posts_per_page'=> -1
            );
            $loop = new WP_Query($args);
?>
<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <?php the_content(); ?>
            </div>
            
            <div class="row">
                <?php
                if($loop->have_posts()):
                    while($loop->have_posts()) : $loop->the_post();
                        $title = get_the_title();
                        $content = get_the_content();
                        $link = get_permalink();
                        if($content)
                            echo '<div class="col-md-3"><a href="'. $link .'" class="tag2"><div>'. $title .'</div></a></div>';                                                    
                        else 
                            echo '<div class="col-md-3"><div class="tag2"><div>'. $title .'</div></div></div>';
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<?php
        endwhile;
    endif;
get_footer();
?>