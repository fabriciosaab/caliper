    <a href="#0" class="cd-top" title="Para o topo">Top</a>
    <div class="footer">
        <div class="container pt-3 pb-3">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <?php
                    if(function_exists('psn_newsletter')) {
                        psn_newsletter();
                    }
                    ?>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 pe-5 ps-5 mt-4">
                            <div class="mb-3">
                                <img class="logo-rodape" src="<?php echo URL_TEMPLATE; ?>/images/icon_canal_paciente.svg">
                            </div>
                            <div class="set-atend">
                                ATENDIMENTO EXCLUSIVO PARA PACIENTES QUE DESEJAM INFORMAÇÕES SOBRE EXAMES.
                            </div>
                            <div class="set-tel">
                                (71) 99245-2399
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 pe-5 ps-5 mt-4">
                            <div class="mb-3">
                                <img class="logo-rodape" src="<?php echo URL_TEMPLATE; ?>/images/icon_canal_aluno.svg">
                            </div>
                            <div class="set-atend">
                                ATENDIMENTO EXCLUSIVO PARA MÉDICOS QUE DESEJAM INFORMAÇÕES SOBRE CURSOS.
                            </div>
                            <div class="set-tel">
                                (71) 3014-6040<br>
                                (71) 99341-1122
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 set-horario">
                            HORÁRIOS DE ATENDIMENTO<br>
                            SEGUNDA À SEXTA, DAS 07H AS 19H E SÁBADO DAS 07H ÀS 13H
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.footer -->
        <div style="background: #d71921; padding: 10px; font-size: 1.2em; color:#fff">
            <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-3 text-center">
                    <img class="logo-rodape" src="<?php echo URL_TEMPLATE; ?>/images/caliper_logo_bco.svg">
                </div>
                <div class="col-sm-12 col-md-8 col-lg-9 text-sm-center text-md-start pt-sm-1 pt-md-4">
                    <?php
                    $text_footer = get_field('text_footer','options');
                    if($text_footer):
                        foreach($text_footer as $text):
                            echo '<p>'. $text["text"] .'</p>';
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
            </div>
        </div>
    </div>

<!-- Widget Whatsapp -->
<div class="widget-whatsapp-btn">
    <i class="fa fa-whatsapp" aria-hidden="true"></i>
</div>

<div id="widget-whatsapp">
    <div class="container">
        <div class="row bg-azul">
            <div class="col p-3 text-center">
                <p class="m-0">Olá Preencha os campos abaixo para iniciar a conversa no WhatsApp.<br>
                    (Apenas sobre o assunto de cursos Caliper)
                </p>
                <button type="button" class="widget-close">X</button>
            </div>
        </div>

        <div class="row bg-cinza">
            <div class="col p-3 text-center">
                <form class="widget-form row" id="widget-form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input id="nameWhats" name="nameWhats" type="text" placeholder="Nome*" class="form-control form-control-sm cont input-md" required="required">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input id="emailWhats" name="emailWhats" type="email" placeholder="E-mail*" class="form-control form-control-sm cont input-md" required="required">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input id="numberWhats" name="numberWhats" type="tel" placeholder="(xx) xxxxx-xxxx" class="form-control form-control-sm cont input-md" required="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <input id="widget-submit" type="submit" class="btn btn-danger w-100" value="Iniciar a conversa">
                    </div>
                </form>
            </div>
        </div>

        <div class="row bg-azul">
            <div class="col p-3 text-center">
                <p class="whats-text"><i class="fa fa-whatsapp" aria-hidden="true"></i> (71) 99341-1122</p>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/jquery.min.js' id='jquery-theme-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/bootstrap.min.js' id='bootstrap-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/menumaker.js' id='menumaker-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/animsition.js' id='animsition-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/animsition-script.js' id='animsition-script-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/slider-carousel.js' id='slider-carousel-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/service-carousel.js' id='service-carouse-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/back-to-top.js' id='back-to-top-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/isotope/isotope.js' id='isotope-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/fancy/jquery.fancybox.pack.js' id='fancybox-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/toastr.js' id='toastr-js'></script>
<script type='text/javascript' src='<?php echo URL_TEMPLATE; ?>/js/functions.js' id='functions-js'></script>
<script type="text/javascript" src="<?php echo URL_TEMPLATE; ?>/js/owl.carousel.min.js"></script>
<script type='text/javascript' src="<?php echo URL_TEMPLATE; ?>/js/widget-whatsapp.js"></script>

<script type='text/javascript' id='functions-js-extra'>
    /* <![CDATA[ */
        var ajaxFormCaliper = {"ajax_url":"<?php echo site_url(); ?>\/wp-admin\/admin-ajax.php"};
    /* ]]> */
</script>

<script type="text/javascript">
    $(window).load(function(){
        var $container = $('.portfolioContainer');
        $container.isotope({
            filter: '*',
        });
    
        $('.portfolioFilter a').click(function(){
            $('.portfolioFilter .current').removeClass('current');
            $(this).addClass('current');
    
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector, 
            });
            return false;
        }); 
    });
</script>

<script>
    jQuery(document).ready(function ($) {
       $('#enviarPreinscricao').on('click', function() {
         ga('send', 'event', 'curso','enviado', 'pre-inscricao');
       });
    });
</script>

<script>
    jQuery(document).ready(function ($) {
       $('#linkPreInscricao').on('click', function() {
         ga('send', 'event', 'linkPreInscricao','interesse', 'link');
       });
    });
</script>

<?php // wp_footer(); ?>

</body>