<?php

//  ADD Content FOOTER
function add_footer($function_to_add, $priority = 9999, $accepted_args = 1){
    add_action("wp_footer", $function_to_add, $priority, $accepted_args);
}

//MENU
add_theme_support( 'menus' );
if ( function_exists( 'register_nav_menu' ) ) {
    register_nav_menu( 'menu', 'Menu Principal' );
    register_nav_menu( 'menu2', 'Menu Rodapé 1' );
    register_nav_menu( 'menu3', 'Menu Rodapé 2' );
}
// THUMBNAIL
if (function_exists('add_theme_support')){
    add_theme_support('post-thumbnails');
    add_image_size('358x140', 358, 140, true);
    add_image_size('554x256', 554, 256, true);
    add_image_size('554x256_NO', 554, 256, array( 'center', 'center' ));
    add_image_size('263x263', 263, 263, true);
    add_image_size('109x162', 109, 162, false);
    add_image_size('300x300', 300, 300, false);
    add_image_size('300x300_CROP', 300, 300, true);
    add_image_size('268x340', 268, 340, false);
    add_image_size('750x410', 750, 410, array( 'center', 'center' ));
    add_image_size('1200x410', 1200, 410, array( 'center', 'center' ));
}

// ACF
require_once('acf.php');

// ASSETS
require_once('enqueue.php');

// MAIL
require_once("mail.php");

// COMPONENTS
require_once("components.php");


?>