<div class="sidebar-area">
    <div class="row">
        <div class="col-md-12">
            <div class="widget outline">
            <h3>Buscar</h3>
            <form action="<?php echo URL_SITE; ?>">
                <div class="input-group">
                    <input type="text" class="form-control" name="s" require="require"  minlength="3" placeholder="Pesquise aqui">
                    <span class="input-group-btn">
                    <button class="btn btn-danger" type="button"><i class="fa fa-search"></i></button>
                    </span> </div>
                <!-- /input-group --> 
                </div>
            </form>
        </div>
        
        <div class="col-md-12">
            <div class="widget outline">
                <h3>Categories</h3>
                <ul class="listnone bullet bullet-arrow-circle-right">
                    <?php 
                    $args = array('orderby' => 'name', 'order' => 'DESC');
                    $categories = get_terms('category', $args );
                    if($categories) :
                        
                        foreach($categories as $category) :
                            $cat_id     = $category->term_id;
                            $cat_name   = $category->name;
                            $cat_slug   = $category->slug;
                            echo '<li><a href="'. get_category_link($cat_id) .'">'. $cat_name .'</a></li>';
                        endforeach;
                    endif;
                    ?>
                </ul>
            </div>
        </div>
        <?php
        $args = array(
            'post_type'=> 'post',
            'posts_per_page'=> 5,
            'meta_key' => 'wpb_post_views_count', 
            'orderby' => 'meta_value_num', 
            'order' => 'DESC' 
        );
        $loop = new WP_Query($args);
        if ($loop->have_posts()) :
        ?>
            <div class="col-md-12">
                <div class="widget outline">
                    <h3 class="mb30">Mais Vistos</h3>
                    <ul class="listnone">
                        <?php
                        while($loop->have_posts()) : $loop->the_post();
                            $title = get_the_title();
                            $link = get_permalink();
                            $url_youtube = get_field('url_youtube');
                            $image = get_the_post_thumbnail( $post->ID , 'thumbnail', array( 'class' => 'img-responsive mb10' ) );
                        ?>
                            <li>
                                <div class="recent-block mb20">
                                    <div class="row">
                                    <div class="col-md-4 col-sm-2 col-xs-5"> 
                                        <a href="<?php echo $link; ?>">
                                            <?php
                                            if($url_youtube):
                                                parse_str( parse_url( $url_youtube, PHP_URL_QUERY ), $parameter );
                                                if(!$image)
                                                    $image = '<img src="https://i1.ytimg.com/vi/'. $parameter["v"] .'/0.jpg" alt="'. $title .'" class="img-responsive mb10">';
                                            endif;
                                            echo $image;
                                            ?>
                                            
                                        </a>
                                    </div>
                                    <div class="col-md-8 col-sm-10 col-xs-7">
                                        <h4 class="recent-title mb10"><a href="<?php echo $link; ?>" class="title"><?php echo $title; ?></a></h4>
                                        <div class="meta"> 
                                        <!-- post meta --> 
                                        <span class="meta-date"><?php the_time('d \d\e F \d\e Y'); ?></span> </div>
                                        <!-- /.post meta --> 
                                    </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                        endwhile;
                        ?>
                    </ul>
                </div>
            </div>
        <?php
        endif;
        ?>
    </div>
</div>