<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            $gallery = get_field('gallery');
            get_template_part( 'incs/partial/partial', 'box-title' );
?>

<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <?php the_content(); ?>
                <p>&nbsp;</p>
            </div>
        </div>
        
        <?php if($gallery): ?>
            <div class="row">
                <?php foreach ($gallery as $image): ?>
                    <div class="col-md-3 mb20">    
                        <a class="fancybox" href="<?php echo $image['url']; ?>" data-fancybox-group="gallery">
                            <img src="<?php echo $image['sizes']['300x300']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive img-thumbnail">
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php
        endwhile;
    endif;
get_footer();
?>