<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            get_template_part( 'incs/partial/partial', 'box-title' );
            $args = array(
                'post_type'=> 'lead_content',
                'posts_per_page'=> -1
            );
            $loop = new WP_Query($args);
?>
            <!-- content start -->
            <div class="bg-white">
                <div class="container pt-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="section-about">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php //the_content(); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <?php
                                    if($loop->have_posts()):
                                        while($loop->have_posts()) : $loop->the_post();
                                            get_lead_content_box();
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
        endwhile;
    endif;
get_footer();
?>