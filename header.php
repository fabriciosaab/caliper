<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="YhXtl5_3cw8ucJZrEkoM8snEEWAE-Um7_g_4QRswZAY" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="robots" content="index,follow">
    <link rel="icon" href="<?php echo URL_TEMPLATE; ?>/images/favicon.png">
    <link rel="canonical" href="http://www.escolacaliper.com.br/" />
    <link rel="canonical" href="http://www.escolacaliper.com.br/institucional/nossa-historia/" />
    <link rel="canonical" href="http://www.escolacaliper.com.br/cursos/programa-de-treinamento-profissional-em-ecocardiografia-fetal-nos-moldes-de-pos-graduacao/" />
    
    <title><?php wp_title('&laquo;', true, 'right'); ?><?php bloginfo('name'); ?></title>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-40647735-1', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '421569471362609');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=421569471362609&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

    <?php if(function_exists('lead_modal')) lead_modal(); ?>

    <?php // wp_head(); ?>

    <meta name='robots' content='max-image-preview:large' />

    <link rel="canonical" href="http://localhost:90/contato/" />
    <meta property="article:publisher" content="https://www.facebook.com/caliperescoladeimagem/" />

    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
            
    <link rel='stylesheet' id='bootstrap-css'  href='<?php echo URL_TEMPLATE; ?>/css/bootstrap.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css'  href='<?php echo URL_TEMPLATE; ?>/css/font-awesome.min.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo URL_TEMPLATE; ?>/css/owl.theme.css">
    <link rel='stylesheet' id='fontello-css'  href='<?php echo URL_TEMPLATE; ?>/css/fontello.css' type='text/css' media='all' />
    <link rel="stylesheet" href="<?php echo URL_TEMPLATE; ?>/css/owl.carousel.css">
    <link rel='stylesheet' id='fancybox-css'  href='<?php echo URL_TEMPLATE; ?>/js/fancy/jquery.fancybox.css' type='text/css' media='all' />
    <link rel='stylesheet' id='fonts-google-css'  href='https://fonts.googleapis.com/css?family=Lato%3A300%2C400%2C400i%2C700%2C700i%2C900&#038;ver=5.7.3' type='text/css' media='all' />
    
    <link rel='stylesheet' id='style-caliper-css'  href='<?php echo URL_TEMPLATE; ?>/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='<?php echo URL_TEMPLATE; ?>/css/style.css' type='text/css' media='all' />
    <link rel='stylesheet' id='style-css'  href='<?php echo URL_TEMPLATE; ?>/css/widget-whatsapp.css' type='text/css' media='all' />

</head>
<body>
    <div class="collapse searchbar" id="searchbar">
        <div class="search-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Buscar...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Buscar</button>
                            </span>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <!-- TOPB --------------------------------------------------------------------------------------------------------------------------------- -->

    <div class="top-bar"> 
        <!-- top-bar -->
        <div class="container">
            <div class="row" style="margin-top: 8px;">
                <ul class="list-group list-group-horizontal flex-row-reverse">                    
                    <li class="list-group-item" style="background-color:transparent; border:none; color:#fff;">
                        <img class="icon-canal-aluno" src="<?php echo URL_TEMPLATE; ?>/images/icon_aluno_bco.svg">
                        <strong>Canal do Aluno </strong>(cursos) (71) 3014-6040 ou
                        <i class="fa fa-whatsapp ms-2" style="font-size: 15px;"></i> (71) 99341-1122 
                        <a href="https://www.facebook.com/caliperescoladeimagem/"><i class="fa fa-facebook ms-3 me-3" style="font-size: 15px;"></i></a>
                        <a href="https://www.instagram.com/escolacaliper/"><i class="fa fa-instagram" style="font-size: 15px;"></i></a>
                        <a href="https://www.youtube.com/channel/UCGXwxei09UUPe_e9lA2JiSg"><i class="fa fa-youtube-play ms-3" style="font-size: 17px;"></i></a>
                    </li>
                    <li class="list-group-item" style="background-color:transparent; border:none; color:#fff;">
                        <img class="icon-canal-paciente" src="<?php echo URL_TEMPLATE; ?>/images/icon_paciente_bco.svg">
                        <strong>CANAL DO PACIENTE</strong> (71)&nbsp;99245-2399
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12"> 
                    <div class="logo">
                        <a href="<?php echo URL_SITE; ?>"><img src="<?php echo URL_TEMPLATE; ?>/images/caliper_logo.svg" alt="Caliper"></a>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <?php
                    $menuParams=array(
                        'theme_location' => 'menu',
                        'echo' => false,
                        'container_class'=>'',
                        'container_id'=>'navigation',
                        'menu_class'=>'',
                        'container'=>'div',
                        'before' => '',
                        'after' => '',
                        'items_wrap' => '<ul data-in="fadeInDown" data-out="fadeOutUp" id="%1$s" class="menu-class%2$s">%3$s</ul>',
                    );

                    $menu = wp_nav_menu($menuParams);
                    print ($menu)?$menu:"";
                    ?>
                </div>
            </div>
        </div>
    </div>