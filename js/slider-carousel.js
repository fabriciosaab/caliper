// Template slider jQuery script

$(document).on('ready', function() {
    "use strict";
    if (jQuery().owlCarousel) {
        $(".slider").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 3000,
            paginationSpeed: 400,
            singleItem: true,
            pagination: true,
            autoPlay: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            addClassActive: true,


            // "singleItem:true" is a shortcut for:
            // items : 1, 
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });
        
        
        $(".slide-depoimentos").owlCarousel({
            navigation: true, // Show next and prev buttons
            slideSpeed: 800,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false,
            autoPlay: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            addClassActive: false,


            // "singleItem:true" is a shortcut for:
            // items : 1, 
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });
    }

});
