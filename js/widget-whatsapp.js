console.log('widget-whatsapp');

$('#widget-submit').click( function(e) {
	e.preventDefault();

	let name   = $('#nameWhats').val();
	let email  = $('#emailWhats').val();
	let number = $('#numberWhats').val();

	let message = "Olá meu nome é " + name + " e gostaria de falar sobre cursos."
	
	let target = `https://api.whatsapp.com/send?phone=5571993411122&text=${encodeURIComponent(message)}`

	console.log(target);

  	window.open(target, '_blank');
});

$('.widget-whatsapp-btn').click( function(e) {
	$('#widget-whatsapp').show();
});

$('.widget-close').click( function(e) {
	$('#widget-whatsapp').hide();
});