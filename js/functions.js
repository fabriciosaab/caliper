var $toastlast = null,
    $toast = '';
$(document).ready(function() {

    $(".fancybox , .fancybox-testimonial").fancybox({
        transitionIn: 'none',
        transitionOut: 'none',
        afterClose: function() {
            $('body').addClass('fade-in');
        }
    });
    if (jQuery().toastr) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "onclick": null
        }
    }
    $(document).on('submit', '#contact-form', function(event) {
        event.preventDefault();
        _form = $(this);
        _data = _form.serialize();
        _formBtn = _form.find('input[type="submit"]');
        _formBtn.addClass('disabled');
        $.ajax({
            url: ajaxFormCaliper.ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data: _data,
            beforeSend: function() {
                notificationInfo("Enviando", "Estamos enviando a seu contato");
            },
            success: function(data, status) {

                // resposta
                $_resposta = data;
                // verifica se existe
                if ($_resposta) {
                    var _cod, _message;
                    // quebrando os resultados
                    // obtendo o cod
                    _cod = $_resposta.cod;
                    _message = $_resposta.message;
                    if (_cod == 1)
                        notificationSuccess('Sucesso', _message);
                    else
                        notificationError('Error', _message);
                    _formBtn.removeClass('disabled');

                    setTimeout(function() {
                        // limpa os campos
                        _form[0].reset();
                    }, 3000);

                } else {
                    notificationError('Error', 'Não enviado');
                }

            },
            error: function(error) {
                alert('Error!');
                _formBtn.removeClass('disabled');
            }
        })
    });

    $(document).on('submit', '[data-form="newsletter"]', function(event) {
        event.preventDefault();
        _obj = $(this);
        _data = _obj.serialize();
        _objBtn = _obj.find('input[type="submit"]');
        _objBtn.addClass('disabled');


        $.ajax({
            url: ajaxFormCaliper.ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data: _data,
            beforeSend: function() {
                notificationInfo('Cadastrando', 'Estamos cadastrando o seu e-mail!');
            },
            success: function(data, status) {
                // resposta
                $_resposta = data;
                // verifica se existe
                if ($_resposta) {
                    var _cod, _message;
                    // quebrando os resultados
                    // obtendo o cod
                    _cod = $_resposta.cod;
                    _message = $_resposta.message;

                    if (_cod == 1) {
                        notificationSuccess('Cadastrado', 'O seu e-mail foi cadastrado com sucesso!');
                    } else if (_cod == 2) {
                        notificationError('Erro', _message);
                    } else if (_cod == 3) {
                        notificationError('Erro', _message);
                    } else {
                        notificationError('Erro', _message);
                    }
                    $('[data-form="newsletter"]')[0].reset();
                    _objBtn.removeClass('disabled');
                } else {
                    alert('Não foi possível realizar seu cadastro entre em contato com contato@tramandosuafesta.com.br');
                }

            },
            error: function(error) {
                alert('Error!');
                _objBtn.removeClass('disabled');
            }
        })
    });

    $(document).on('submit', '[data-form="inscription"]', function(event) {
        event.preventDefault();
        _obj = $(this);
        _data = _obj.serialize();
        _objBtn = _obj.find('input[type="submit"]');
        _objBtn.addClass('disabled');


        $.ajax({
            url: ajaxFormCaliper.ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data: _data,
            beforeSend: function() {
                notificationInfo('Pré Inscrição', 'Estamos fazendo sua pré inscrição!');
            },
            success: function(data, status) {
                // resposta
                $_resposta = data;
                // verifica se existe
                if ($_resposta) {
                    var _cod, _message;
                    // quebrando os resultados
                    // obtendo o cod
                    _cod = $_resposta.cod;
                    _message = $_resposta.message;

                    if (_cod == 1) {
                        notificationSuccess('Pré Inscrição', 'Pré inscrição foi realizada com sucesso!');
                    } else if (_cod == 2) {
                        notificationError('Erro', _message);
                    } else if (_cod == 3) {
                        notificationError('Erro', _message);
                    } else {
                        notificationError('Erro', _message);
                    }
                    $('[data-form="inscription"]')[0].reset();
                    _objBtn.removeClass('disabled');
                } else {
                    alert('Não foi possível realizar sua pré inscrição entre em contato com contato@escolacaliper.com.br');
                }

            },
            error: function(error) {
                alert('Error!');
                _objBtn.removeClass('disabled');
            }
        })
    });

    $(document).on('submit', '[data-form="register-lead"]', function(event) {
        event.preventDefault();
        _obj = $(this);
        _data = _obj.serialize();
        _objBtn = _obj.find('input[type="submit"]');
        _objBtn.addClass('disabled');


        $.ajax({
            url: ajaxFormCaliper.ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data: _data,
            beforeSend: function() {
                notificationInfo('Liberando', 'Estamos autorizando o seu material!');
            },
            success: function(data, status) {
                // resposta
                $_resposta = data;
                // verifica se existe
                if ($_resposta) {
                    var _cod, _message;
                    // quebrando os resultados
                    // obtendo o cod
                    _cod = $_resposta.cod;
                    _message = $_resposta.message;

                    if (_cod == 1 || _cod == 5 || _cod == 2) {
                        notificationSuccess('Liberado', 'Seu Material estará disponível em seguida!');

                        if ($_resposta.url) {
                            $.fancybox.open({
                                src: $_resposta.url,
                                afterClose: function() {
                                    parent.$.fancybox.close();
                                }
                            });
                            $('body').attr('class', '');
                        }
                    } else if (_cod == 3) {
                        notificationError('Erro', 'Entre em contato com nossa equipe!');
                    } else {
                        notificationError('Erro', 'Entre em contato com nossa equipe!');
                    }
                    $('[data-form="register-lead"]')[0].reset();
                    _objBtn.removeClass('disabled');
                } else {
                    alert('Não foi possível realizar sua pré inscrição entre em contato com contato@escolacaliper.com.br');
                }

            },
            error: function(error) {
                alert('Error!');
                _objBtn.removeClass('disabled');
            }
        })
    });

    $(document).on('click', '.botao-barra', function() {
        event.preventDefault();
        _obj = $(this);
        _container = _obj.parents('.barra-lateral');
        if (!_obj.hasClass('active')) {
            _obj.addClass('active');
            _container.addClass('active');
        } else {
            _obj.removeClass('active');
            _container.removeClass('active');
        }
    })

    $('.conteudo-txt a').fancybox({
        type: 'iframe',
        maxWidth: 800,
        maxHeight: 600,
        fitToView: false,
        width: '70%',
        height: '70%',
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none'
    });
})


function notificationSuccess(title, msg, link, text) {
    getLastToast();

    if (link) {
        $(link + ' .modal-body').html(text);
        modalNotification(link);
    }

    if (title && msg) {
        $toast = toastr.success(msg, title);
    } else if (title) {
        $toast = toastr.success('Success', title);
    } else if (msg) {
        $toast = toastr.success(msg, 'Data updated');
    } else {
        $toast = toastr.success('Success', 'Data updated');
    }

    $toastlast = $toast;
    return false;
}

function notificationError(title, msg, link, text) {

    getLastToast();

    if (link) {
        $(link + ' .modal-body').html(text);
        modalNotification(link);
    }

    if (title && msg) {
        $toast = toastr.error(msg, title);
    } else if (title) {
        $toast = toastr.error('Try again', title);
    } else if (msg) {
        $toast = toastr.error(msg, 'Data not updated');
    } else {
        $toast = toastr.error('Try again', 'Data not updated');
    }

    $toastlast = $toast;
    return false;
}

function notificationInfo(title, msg, link, text) {
    getLastToast();

    if (link) {
        $(link + ' .modal-body').html(text);
        modalNotification(link);
    }

    $toast = toastr.info(msg, title);


    $toastlast = $toast;
    return false;
}

function notificationAlert(title, msg, link, text) {
    getLastToast();

    if (link) {
        $(link + ' .modal-body').html(text);
        modalNotification(link);
    }

    $toast = toastr.warning(msg, title);


    $toastlast = $toast;
    return false;
}

function getLastToast() {
    if ($toastlast) {
        toastr.clear($toastlast);
    }
    toastr.options.onclick = null;
}

function modalNotification(Link) {
    toastr.options.onclick = function() {
        $(Link).modal()
    };
}