<?php
get_header();
    get_template_part( 'incs/partial/partial', 'box-title' );
    if (have_posts()) :
        while(have_posts()) : the_post();
            $gallery = get_field('gallery');
            $author = get_the_author_meta('display_name');
            $title = get_the_title();
            $link = get_permalink();
            $url_youtube = get_field('url_youtube');
            $image = get_the_post_thumbnail( $post->ID , '750x410', array( 'class' => 'img-responsive' ) );
?>

<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <article class="post-holder">
                            <div class="post-block mb40">
                                <div class="post-img"> 
                                    <?php
                                    if($url_youtube):
                                        parse_str( parse_url( $url_youtube, PHP_URL_QUERY ), $parameter );
                                    ?>
                                        <div class="embed-responsive">
                                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $parameter['v']; ?>"></iframe>
                                        </div>	
                                    <?php
                                    else:
                                    ?>
                                        <?php echo '<a href="#" class="imghover">'. $image .'</a>'; ?>
                                    <?php
                                    endif;
                                    ?>
                                </div>
                                <div class="bg-white">
                                    <h2>
                                        <a href="#" class="title"><?php echo $title; ?></a>
                                    </h2>
                                    <p class="meta">
                                        <span class="meta-date"><?php the_time('d \d\e F \d\e Y') ?></span>
                                        <span class="meta-author">Autor:<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="meta-link"> <?php echo $author; ?></a></span>
                                    </p>
                                    <?php the_content(); ?>
                                    <?php
                                    if($gallery):
                                    ?>
                                        <div class="row">
                                            <?php
                                            foreach ($gallery as $image):
                                            ?>
                                                <div class="col-md-3 mb20">    
                                                    <a class="fancybox" href="<?php echo $image['url']; ?>" data-fancybox-group="gallery">
                                                        <img src="<?php echo $image['sizes']['300x300_CROP']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive img-thumbnail">
                                                    </a> 
                                                </div>
                                            <?php 
                                            endforeach;
                                            ?>
                                        </div>
                                    <?php
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
        endwhile;
    endif;
get_footer();
?>