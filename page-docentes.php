<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            get_template_part( 'incs/partial/partial', 'box-title' );
            $args = array(
                'post_type'=> 'teams',
                'posts_per_page'=> -1
            );
            $loop = new WP_Query($args);
?>
<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <?php the_content(); ?>
            </div>
            
            <div class="row">
                <?php
                if($loop->have_posts()): ?>
                    <?php while($loop->have_posts()) : $loop->the_post();
                        $idPost = get_the_ID();
                        $title = get_the_title();
                        $image = get_the_post_thumbnail( $idPost , '268x340', array( 'class' => 'img-responsive' ) );
                    ?>
                    <div class="col-md-3">
                        <a class="fancybox" href="#<?php echo $idPost; ?>" data-fancybox-group="gallery">
                            <?php echo $image; ?>
                            <div class="tag">
                                <div><?php echo $title; ?></div>
                            </div>
                        </a>
                        <div id="<?php echo $idPost; ?>" class="fancy-modal">     
                            <?php
                            if($image):
                            ?>
                                <div class="col-md-3">
                                    <?php echo $image; ?>
                                </div>
                            <?php
                            endif;
                            ?>                                                
                            <div class="col-md-9">
                                <h3><?php echo $title; ?></h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
        endwhile;
    endif;
get_footer();
?>