<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            get_template_part( 'incs/partial/partial', 'box-title' );
?>
<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                    <?php the_content(); ?>
                    <?php 
                        if(get_the_content())
                            echo '<hr>';
                    ?>
                </div>
            </div>

            <?php
            $slugTerm = 'category_interns';
            $args = array(
                'taxonomy' => $slugTerm,
                'hide_empty' => true,
                'order' => 'ASC',
                'orderby' => 'description'
            ); 
            $categories = get_terms($args);
            if($categories):
                $count = 0;
                if($count > 0)
                    echo '<hr>';
                foreach($categories as $category):
            ?>
                    <div class="row md60">
                        <div class="col-md-12">
                            <h3><?php echo $category->name; ?></h3>
                        </div>
                        <?php
                        $args = array(
                            'post_type'=> 'interns',
                            'posts_per_page'=> -1,
                            $slugTerm => $category->slug
                        );
                        $loop = new WP_Query($args);
                        if($loop->have_posts()):
                            while($loop->have_posts()) : $loop->the_post();
                                $title = get_the_title();
                                $image = get_the_post_thumbnail( $post->ID , '268x340', array( 'class' => 'img-responsive' ) );
                        ?>
                            <div class="col-md-3">
                                <?php echo $image; ?>
                                <div class="tag">
                                    <div><?php the_title(); ?></div>
                                </div>
                            </div>
                        <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
            <?php
                    $count++;
                endforeach;
            endif;
            ?>
        </div>
    </div>
</div>

<?php
        endwhile;
    endif;
get_footer();
?>