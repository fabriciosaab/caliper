<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();

            $idCourse = get_the_ID();

            $image = get_field('image_featured');
            $image = $image['sizes']['1200x410'];

            $category     = get_the_terms( get_the_ID(), 'category_course' );
            $color_button = get_field("color_button", $category[0]);

            // Aba informações
            $load_time = get_field('load_time');
            $duracao   = get_field('duracao');
            $programa  = get_field('programa');

            // Aba Programação
            $investimento = get_field('investimento');
            $indicado     = get_field('indicado_para');
            $preRequisito = get_field('pre_requisito');
            $objetivos    = get_field('objetivos');
            $diferenciais = get_field('diferenciais');
            $teams        = get_field('professores');
?>

<script>
    fbq('track', 'ViewContent', {
        value: 3.50,
        currency: 'USD'
    });
</script>

<div class="w-100" style="background: url(<?php echo ($image != false ? $image : '' ); ?>) no-repeat center; background-size: cover;height: 150px;">
</div>

<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center mt30 mb30">
                <h1 class="curso<?php echo $color_button; ?>-titulo"><?php the_title(); ?></h1>
                <?php
                if($load_time)
                    echo '<p><strong>Carga Horária: ' . $load_time . '</strong></p>';
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <a href="#" class="btn btn-primary btn-lg mb30" style="padding: 15px 50px; background:#3c4d6b">Download calendário completo</a>

                <div class="box-inscricao mb-5">
                    <div class="box-inscricao-header text-center">
                        <h3>Pré-inscrição</h3>
                    </div>

                <form data-form="inscription" id="#inscricao<?php echo $u->post_name; ?>" >
                    <input type="hidden" name="action" value="inscription_register">
                    <?php wp_nonce_field('form_inscriptions', 'verify'); ?>
                    <input type="hidden" name="course_id" value="<?php echo $idCourse; ?>">
                    <input type="hidden" name="unit_id" value="<?php echo $u->ID; ?>">

                    <div class="box-inscricao-body">
                        <div class="form-group">
                            <input type="text" class="form-control" name="nameInscriptions" id="nome" required="required" placeholder="Nome completo:*">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="crmInscriptions" id="crm" required="required" placeholder="CRM:*">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="mailInscriptions" id="email" required="required" placeholder="E-mail:*">
                        </div>
                        
                            <input type="tel" class="form-control" id="cel" name="cellInscriptions" required="required" placeholder="Celular:*">
                        
                        <div class="form-group">
                            <select class="form-select" id="especialidade" name="especialidadeInscriptions" required="required">
                                <option selected>Especialidade</option>
                                <option value="ginecologia">Ginecologia</option>
                                <option value="obstetricia">Obstetrícia</option>
                                <option value="clinico-geral">Clínico Geral</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-select" id="sabendo" name="sabendoInscriptions" required="required">
                                <option selected>Como ficou sabendo do curso</option>
                                <option value="redes-sociais">Redes Sociais</option>
                                <option value="google">Google</option>
                                <option value="site-caliper">Site Caliper</option>
                                <option value="indicacao">Indicação</option>
                                <option value="email-marketing">Email marketing</option>
                            </select>
                        </div>
                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-success btn-enviar-inscricao" id="enviarPreinscricao" onClick="ga('send', 'event', 'curso','enviado', 'pre-inscricao');">Tenho interesse</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col-sm-12 col-md-8">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item me-3" role="presentation">
                    <button class="nav-link active" id="informacoes-tab" data-bs-toggle="tab" data-bs-target="#informacoes" type="button" role="tab" aria-controls="informacoes" aria-selected="true">Informações</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="programacao-tab" data-bs-toggle="tab" data-bs-target="#programacao" type="button" role="tab" aria-controls="programacao" aria-selected="false">Programação</button>
                </li>
            </ul>

            <div class="tab-content mt-4" id="myTabContent">
                <div class="tab-pane fade show active" id="informacoes" role="tabpanel" aria-labelledby="informacoes-tab">
                    <?php
                    if($programa):
                        echo '<img class="mb-2" src="' . URL_TEMPLATE . '/images/icon_programa.svg" style="width:170px">';
                        echo $programa;
                    endif;
                    ?>
                </div>
                
                <div class="tab-pane fade" id="programacao" role="tabpanel" aria-labelledby="programacao-tab">
                    <?php
                    if($investimento):
                        echo '<div class="mt-3 mb-3">';
                        echo '<img class="me-3" src="' . URL_TEMPLATE . '/images/icon_investimento.svg" style="width:200px">';
                        echo $investimento;
                        echo '</div>';
                    endif;
                    
                    if($load_time and $duracao):
                        echo '<strong>Carga horária: </strong>' . $load_time . ' | <strong>Duração: </strong>' . $duracao ;
                    endif;

                    if($indicado):
                        echo '<div class="mt-4 text-blue">';
                        echo '<strong>INDICADO PARA</strong><br>';
                        echo $indicado;
                        echo '</div>';
                    endif;

                    if($preRequisito):
                        echo '<div class="mt-4 text-blue">';
                        echo '<strong>PRÉ-REQUESITO</strong><br>';
                        echo $preRequisito;
                        echo '</div>';
                    endif;

                    if($objetivos):
                        echo '<div class="mt-4 text-blue">';
                        echo '<strong>OBJETIVOS</strong><br>';
                        echo $objetivos;
                        echo '</div>';
                    endif;

                    if($diferenciais):
                        echo '<div class="mt-4 text-blue">';
                        echo '<strong>DIFERENCIAIS</strong><br>';
                        echo $diferenciais;
                        echo '</div>';
                    endif;

                    if($teams):
                        echo '<img class="mt-5" src="' . URL_TEMPLATE . '/images/icon_professores.svg" style="width:200px">';
                        ?>
                        <div class="row mb40 mt-4">
                            <?php
                            foreach ($teams as $t): 
                                $content = '';
                                $idTeam = $t->ID;
                                $title = get_the_title($idTeam);
                                $image = get_the_post_thumbnail( $idTeam , 'thumbnail', array( 'class' => 'img-circle' ) );
                                $image2 = get_the_post_thumbnail( $idTeam , '268x340' );
                                $content = apply_filters('the_content', get_post_field('post_content', $idTeam));
                                $crm = get_field('crm',$idTeam);
                                ?>
                                <div class="col mb10">
                                    <div class="text-center">
                                        <?php echo $image; ?>
                                    </div>
                                    <div class="rox text-center">
                                        <strong><?php echo $title; ?></strong>
                                        <br>
                                        <?php
                                        if($content):
                                        ?>
                                            <a class="fancybox" href="#<?php echo $idTeam; ?>" ddata-fancybox-group="gallery-<?php echo $u->post_name; ?>">
                                                Ver Currículo
                                            </a>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php if($content): ?>
                                    <div id="<?php echo $idTeam; ?>" class="fancy-modal">
                                        <div class="col-md-3">
                                            <?php echo $image2;?>
                                        </div>
                                        
                                        <div class="col-md-9">
                                            <h3><?php echo $title; ?></h3>
                                            <?php echo $content; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Código do Google para tag de remarketing -->
<!--------------------------------------------------
As tags de remarketing não podem ser associadas a informações pessoais de identificação nem inseridas em páginas relacionadas a categorias de confidencialidade. Veja mais informações e instruções sobre como configurar a tag em: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 943735835;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/943735835/?guid=ON&amp;script=0"/>
</div>
</noscript>


<?php
endwhile;
endif;

get_footer();
?>