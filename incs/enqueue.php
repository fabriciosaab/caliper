<?php
//Template Start
function startup_psn(){
    add_action('wp_enqueue_scripts', 'scripts_pns', 2);
    add_action('wp_enqueue_scripts', 'style_psn',2);
}
add_action('after_setup_theme','startup_psn');
function scripts_pns() {
    if(is_admin())
        return;
    global $template_caliper;
    wp_enqueue_script( 'jquery-theme' , $template_caliper["url_template"] . '/js/jquery.min.js', array(), '3.0', true );
    wp_enqueue_script( 'bootstrap' , $template_caliper["url_template"] . '/js/bootstrap.min.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'menumaker' , $template_caliper["url_template"] . '/js/menumaker.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'animsition' , $template_caliper["url_template"] . '/js/animsition.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'animsition-script' , $template_caliper["url_template"] . '/js/animsition-script.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'slider-carousel' , $template_caliper["url_template"] . '/js/slider-carousel.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'service-carouse' , $template_caliper["url_template"] . '/js/service-carousel.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'back-to-top' , $template_caliper["url_template"] . '/js/back-to-top.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'isotope' , $template_caliper["url_template"] . '/js/isotope/isotope.js', array( 'jquery-theme' ), '3.0', true );
    wp_enqueue_script( 'fancybox' , $template_caliper["url_template"] . '/js/fancy/jquery.fancybox.pack.js', array( 'jquery-theme' ), '2.15', true );
    wp_enqueue_script( 'toastr' , $template_caliper["url_template"] . '/js/toastr.js', array( 'jquery-theme' ), '2.15', true );
    wp_enqueue_script( 'functions' , $template_caliper["url_template"] . '/js/functions.js', array( 'jquery-theme' ), '2.15', true );
    wp_localize_script( 'functions', 'ajaxFormCaliper', array(
		'ajax_url' => admin_url( 'admin-ajax.php' )
	));
}
function style_psn(){
    if(is_admin())
        return;
    
    global $template_caliper;
    wp_enqueue_style( 'bootstrap', $template_caliper["url_template"] . '/css/bootstrap.min.css', array());
    wp_enqueue_style( 'font-awesome', $template_caliper["url_template"] . '/css/font-awesome.min.css', array());
    wp_enqueue_style( 'style', $template_caliper["url_template"] . '/css/style.css', array());
    wp_enqueue_style( 'fontello', $template_caliper["url_template"] . '/css/fontello.css', array());
    wp_enqueue_style( 'animsition', $template_caliper["url_template"] . '/css/animsition.min.css', array());
    wp_enqueue_style( 'fancybox', $template_caliper["url_template"] . '/js/fancy/jquery.fancybox.css', array());
    wp_enqueue_style( 'style-caliper', $template_caliper["url_template"] . '/style.css', array());
    wp_enqueue_style( 'fonts-google', 'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i,900', array());
}
?>
