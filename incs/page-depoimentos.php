<?php
get_header();
wp_reset_postdata();
wp_reset_query();
get_template_part( 'incs/partial/partial', 'box-title' );
$post_per_page = 6;

$args = array(
    'post_type'=> 'testimonials',
    'posts_per_page'=> $post_per_page ,
    'paged' => $paged,
    'meta_key' => 'type',
    'meta_value' => 'video'
);
$loop = new WP_Query($args);
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="wrapper-content bg-white pinside40">
                    <div class="row cc3">
                        <?php
                            if($loop->have_posts()):
                                while($loop->have_posts()) : $loop->the_post();
                                    $title = get_the_title();
                                    $url_youtube = get_field('url_youtube');
                                    $image = get_the_post_thumbnail( $post->ID , '554x256', array( 'class' => 'img-responsive mb10' ) );
                                    parse_str( parse_url( $url_youtube, PHP_URL_QUERY ), $parameter );
                                    if(!$image)
                                        $image = '<img src="https://i1.ytimg.com/vi/'. $parameter["v"] .'/0.jpg" alt="'. $title .'" class="img-responsive mb10">';
                            ?>
                                    <div class="col-md-4">
                                        <article class="post-holder">
                                            <div class="post-block mb40">
                                                <div class="post-img"> 
                                                    <a href="<?php echo ($url_youtube =! false ? $url_youtube : '#' );?>" class="fancybox-testimonial iframe">
                                                        <?php echo ($image != false ? $image : '' ); ?>
                                                    </a>	
                                                </div>
                                                <div class="bg-white">
                                                    <h1><a href="<?php echo ($url_youtube =! false ? $url_youtube : '#' );?>" class="fancybox-testimonial iframe title"><?php echo $title; ?></a></h1>
                                                    <p class="meta"> <span class="meta-date"><?php the_time('d \d\e F \d\e Y') ?> </span> <span class="meta-author">Autor:<a href="#" class="meta-link"> Caliper</a></span> </p>
                                                    <?php the_content(); ?>
                                                    <a href="<?php echo ($url_youtube =! false ? $url_youtube : '#' );?>" class="fancybox-testimonial iframe btn-link">Saiba mais</a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                            <?php
                                endwhile;
                            endif;
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
?>