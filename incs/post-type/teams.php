<?php 
// Post Type - teams
add_action('init', 'teams');
 
function teams() {
 
    $labels = array(
        'name' => _x('Equipe', 'post type general name'),
        'singular_name' => _x('Equipe', 'post type singular name'),
        'add_new' => _x('Adicionar', 'teams item'),
        'add_new_item' => __('Adicionar'),
        'edit_item' => __('Editar'),
        'new_item' => __('Novo'),
        'view_item' => __('Ver'),
        'search_items' => __('Procurar'),
        'not_found' =>  __('Nenhum encontrado'),
        'not_found_in_trash' => __('Nenhum na lixeira'),
        'parent_item_colon' => '',

    );
 
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'equipes' ),
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-groups',
        'has_archive' => false,
        'supports' => array('title','editor','thumbnail','excerpt')
      );
 
    register_post_type( 'teams' , $args );

    flush_rewrite_rules();
    
    $label = array(
        'name'                  => ('Tipos'),
        'singular_name'         => ('Tipo'),
        'add_new'               => ('Adicionar'),
        'add_new_item'          => ('Adicionar'),
        'edit_item'             => ('Editar'),
        'new_item'              => ('Nova'),
        'view_item'             => ('Ver'),
        'search_items'          => ('Buscar'),
    );

    register_taxonomy('category_team', array('teams'),

    array(
        'labels'                => $label,
        'public'                => true,
        'query_var'             => true,
        'hierarchical'          => true,
        'rewrite'               => array( 'slug' => 'tipo-equipe', 'with_front' => false ))
    );
}
?>