<?php 
// Post Type - banners
add_action('init', 'testimonials');
 
function testimonials() {
 
    $labels = array(
        'name' => _x('Depoimentos', 'post type general name'),
        'singular_name' => _x('Depoimento', 'post type singular name'),
        'add_new' => _x('Adicionar', 'testimonials item'),
        'add_new_item' => __('Adicionar'),
        'edit_item' => __('Editar'),
        'new_item' => __('Novo'),
        'view_item' => __('Ver'),
        'search_items' => __('Procurar'),
        'not_found' =>  __('Nenhum encontrado'),
        'not_found_in_trash' => __('Nenhum na lixeira'),
        'parent_item_colon' => '',

    );
 
    $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'depoimento' ),
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-format-quote',
        'has_archive' => true,
        'supports' => array('title','thumbnail','editor')
      );
 
    register_post_type( 'testimonials' , $args );

    flush_rewrite_rules();

}
?>