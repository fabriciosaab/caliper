<?php 

add_action('init', 'courses');
 
function courses() {
    // Post Type 
    $postTypeInfo = array(
        'name' => 'Cursos',
        'singular_name' => 'Curso',
        'slug' => 'cursos',
        'term' => 'courses',
        'icon' => 'dashicons-calendar-alt',
        'archive' => true,
        'exclude_search' => false
    );
    
    $labels = array(
        'name' => _x($postTypeInfo["name"], 'post type general name'),
        'singular_name' => _x($postTypeInfo["singular_name"], 'post type singular name'),
        'add_new' => _x('Adicionar', 'item'),
        'add_new_item' => __('Adicionar'),
        'edit_item' => __('Editar'),
        'new_item' => __('Novo'),
        'view_item' => __('Ver'),
        'search_items' => __('Procurar'),
        'not_found' =>  __('Nenhum encontrado'),
        'not_found_in_trash' => __('Nenhum na lixeira'),
        'parent_item_colon' => '',
    );
 
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'exclude_from_search' => $postTypeInfo["exclude_search"],
        'query_var' => true,
        'rewrite' => array( 'slug' => $postTypeInfo["slug"] ),
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => $postTypeInfo["icon"],
        'has_archive' => $postTypeInfo["archive"],
        'supports' => array('title','thumbnail')
      );

 
    register_post_type( $postTypeInfo['term'] , $args );

    flush_rewrite_rules();
    
    $label = array(
        'name'                  => ('Segmentos'),
        'singular_name'         => ('Segmento'),
        'add_new'               => ('Adicionar'),
        'add_new_item'          => ('Adicionar'),
        'edit_item'             => ('Editar'),
        'new_item'              => ('Nova'),
        'view_item'             => ('Ver'),
        'search_items'          => ('Buscar'),
    );

    register_taxonomy('category_course', array($postTypeInfo["term"]),

    array(
        'labels'                => $label,
        'public'                => true,
        'query_var'             => true,
        'hierarchical'          => true,
        'rewrite'               => array( 'slug' => 'segmento', 'with_front' => false ))
    );
}
?>