<?php
$featureds = get_field('featureds','options');
if($featureds):
?>
    <div id="chamada" class="section-space60">
        <div class="container">
            <div class="row">
                <?php
                foreach($featureds as $featured):
                ?>
                    <div class="col-md-6 col-sm-6">
                        <a href="<?php echo $featured['link']; ?>"> 
                            <img src="<?php echo $featured['image']['sizes']['554x256']; ?>">
                            <h3><?php echo $featured['title']; ?> <i class="fa fa-arrow-right pull-right"></i></h3>
                        </a>
                    </div>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
<?php
endif;
?>