<?php
$args = array(
    'post_type'=> 'lead_content',
    'posts_per_page'=> 2,
    'meta_key' => 'home',
    'meta_value' => 1
);
$loop = new WP_Query($args);
if ($loop->have_posts()) :
?>
    <div id="conteudo">
        <div class="container pb-5">
            <div class="row">
                <div class="offset-md-2 col-md-8 col-sm-12">
                    <div class="mb60 text-center section-title">
                        <h2>Acesse o Conteúdo Gratuito</h2>
                    </div>
                </div>
            </div>
            <div class="row"> 
                <?php 
                while($loop->have_posts()) : $loop->the_post();
                    get_lead_content_box();
                endwhile;
                ?>
            </div>
            <div class="row">
                <div class="offset-md-4 col-md-4">
                    <a href="<?php echo URL_SITE; ?>/conteudo-gratuito/" class="btn btn-default btn-lg">VER MAIS</a>
                </div>
            </div>
        </div>
    </div>
<?php
endif;
?>