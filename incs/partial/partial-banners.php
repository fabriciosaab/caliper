<div class="slider fade" id="slider"> 
    <?php 
    $args = array(
        'post_type'=> 'banners',
        'posts_per_page'=> 5
    );
    $loop = new WP_Query($args);
    if ($loop->have_posts()) :
        while($loop->have_posts()) : $loop->the_post();
            $link = get_permalink();
            if(has_post_thumbnail):
                $link_external = get_field('link_external');
                
                if($link_external):
                    $link = get_field('url_link');
                else:
                    $link = get_field('link');
                endif;

                echo '<a href="'. $link .'" '. ( get_field("url_link") == true ? 'target="_blank"' : '' ) .'  >';
                    the_post_thumbnail();  
                echo '</a>';
            endif;
        endwhile;
    endif;
    ?>
</div>