<div class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                if(is_singular('post') || is_author()):
                    echo '<h1>Blog</h1>';
                elseif(is_category()):
                    echo '<h1>Blog: '. single_cat_title('',false) .'</h1>';
                elseif(is_search()):
                    echo '<h1>Blog : '. $_GET["s"] .'</h1>';                    
                else:
                    echo '<h1>'. get_the_title() .'</h1>';
                endif;
                ?>
                
            </div>
        </div>
    </div>
</div>