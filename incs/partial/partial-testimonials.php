<?php
$args = array(
    'post_type'=> 'testimonials',
    'posts_per_page'=> 5,
    'meta_key' => 'type',
    'meta_value' => 'text'
);
$loop = new WP_Query($args);

$args2 = array(
    'post_type'=> 'testimonials',
    'posts_per_page'=> 4,
    'meta_key' => 'type',
    'meta_value' => 'video'
);
$loop2 = new WP_Query($args2);
if ($loop->have_posts() || $loop->have_posts()) :
?>
    <div id="depoimentos" class="section-space60">
        <div class="container">
            <div class="row">
                <div class="offset-md-2 col-md-8 col-sm-12 col-xs-12">
                    <div class="mb40 text-center section-title">
                        <h2>Depoimentos</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <?php
            if($loop->have_posts()):
            ?>
                <div class="slide-depoimentos mb40"> 
                    <?php
                    while($loop->have_posts()) : $loop->the_post();
                        $image = get_the_post_thumbnail( $post->ID , 'thumbnail', array( 'class' => 'img-circle' ) );                        
                        $testimonial = get_field('text');
                        $people = get_field('people');
                        $crm = get_field('crm');
                    ?>
                        <div class="row">
                            <div class="col-md-3 col-sm-3 slide-depoimentos-img"> <?php echo ($image != false ? $image : '' );?> </div>
                            <div class="col-md-9 col-sm-9 slide-depoimentos-txt"> <?php echo ($testimonial != false ? $testimonial : '') ;?>  <br><strong><?php echo ($people != false ? $people : '' ); ?></strong><br>
                            <small><?php echo ($crm != false ? $crm : '' );?></small> </div>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            <?php
            endif;
            if($loop2->have_posts()):
            ?>
                <div class="row">
                    <?php
                    while($loop2->have_posts()) : $loop2->the_post();
                        $url_youtube = get_field('url_youtube');
                        $image = get_the_post_thumbnail( $post->ID , '554x256', array( 'class' => 'img-responsive mb10' ) );
                        parse_str( parse_url( $url_youtube, PHP_URL_QUERY ), $parameter );
                        if(!$image)
                            $image = '<img src="https://i1.ytimg.com/vi/'. $parameter["v"] .'/0.jpg" class="img-responsive mb10">';
                    ?>
                        <div class="col-md-3 col-sm-6 col-xs-12 mb60">
                            <a href="<?php echo ($url_youtube =! false ? $url_youtube : '#' );?>" class="imgVideo fancybox iframe">
                                <?php echo ($image != false ? $image : '' ); ?>
                            </a>
                            <a href="<?php echo ($url_youtube =! false ? $url_youtube : '#' );?>" class="fancybox iframe"><?php the_title();?></a>
                        </div>
                    <?php
                    endwhile;
                    ?>
                </div>
            <?php
            endif;
            ?>
            
            <div class="row">
                <div class="col-sm-12 text-center"> <a href="<?php echo URL_SITE; ?>/depoimentos" class="btn btn-primary btn-lg btn-block">VER MAIS</a> </div>
            </div>
        </div>
        
    </div>
<?php
endif;
?>