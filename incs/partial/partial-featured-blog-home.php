<?php
$args = array(
    'post_type'=> 'post',
    'posts_per_page'=> 4,
    'meta_key' => 'home',
    'meta_value' => 1
);
$loop = new WP_Query($args);
if ($loop->have_posts()) :
?>
    <div id="blog" class="section-space60">
        <div class="container">
            <div class="row">
                <div class="offset-sm-2  col-sm-8">
                    <div class="mb60 text-center section-title">
                        <h2>Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php 
                while($loop->have_posts()) : $loop->the_post();
                    $image = get_the_post_thumbnail( $post->ID , '263x263', array( 'class' => 'img-responsive' ) );
                    $link = get_permalink();
                ?>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="post-block mb30">
                            <?php
                            if($image):
                                echo '<div class="post-img">';
                                    echo '<a href="'. $link .'" class="imghover">';
                                        echo $image;
                                    echo '</a>';
                                echo '</div>';
                            endif;
                            ?>
                            <div class="bg-white pinside30 outline"><h3><a href="<?php echo $link; ?>" class="title"><?php the_title(); ?></a></h3>
                                <p class="meta"><span class="meta-date"><?php the_time('d \d\e F \d\e Y')?></span></p>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
    </div>
<?php
endif;
?>