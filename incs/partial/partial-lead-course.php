<div class="barra-lateral" id="mp-pusher" style="transform: translate3d(0px, 0px, 0px);">
    <!-- Botão -->
    <a href="#" class="menu-trigger">
        <!-- <div class="botao-barra rotate" id="botao-barra-lateral" style="background-color: transparent;background-image: url(<?php echo URL_TEMPLATE; ?>/images/banner_lateral.png);">
            Receba mais informacões
        </div> -->

        <div class="botao-barra rotate" id="botao-barra-lateral" style="background-color: transparent;">
            <img class="banner-lateral" src="<?php echo URL_TEMPLATE; ?>/images/banner_lateral.png">
            Receba mais informações
        </div>
    </a>
    <!-- -->
    <div class="mp-pusher">
        <div class="titulo-barra">Receba mais informações sobre os CURSOS DE IMAGEM</div>
         <form data-form="inscription">
            <input type="hidden" name="action" value="inscription_register">
            <?php wp_nonce_field('form_inscriptions', 'verify'); ?>
            <input type="hidden" name="msgInscriptions" value="Feito pela página Inicial">
            <div class="conteudo-barra">
                <p>Cadastre-se e receba informações sobre o curso</p>
                <div class="form-group">
                    <input type="text" required="required" class="form-control" placeholder="Nome" name="nameInscriptions">
                </div>
                <div class="form-group">
                    <input type="email" required="required" class="form-control" placeholder="E-mail" name="mailInscriptions">
                </div>
                <div class="form-group">
                    <input type="tel" required="required" class="form-control" placeholder="Celular" name="cellInscriptions">
                </div>
                <div class="form-group">
                    <select class="form-control" required="required" name="course_id">
                        <option>Escolha o curso</option>
                        <?php 
                        $args = array(
                            'post_type'=> 'courses',
                            'posts_per_page'=> -1
                        );
                        $loop = new WP_Query($args);
                        if ($loop->have_posts()) :
                            while($loop->have_posts()) : $loop->the_post();
                                $title = get_the_title();
                                echo '<option value="'. get_the_ID() .'">'. $title .'</option>';

                            endwhile;
                        endif;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Outro" name="outroInscriptions">
                </div>
                <input type="submit" class="btn btn-danger btn-block" value="Enviar">
            </div>
        </form>
    </div>
    <!-- /mp-menu -->
</div>