<?php
wp_reset_query();
get_template_part( 'incs/partial/partial', 'box-title' );
$post_per_page = get_option( 'posts_per_page' );

if(is_category()):
    $args = array(
        'post_type'=> 'post',
        'posts_per_page'=> $post_per_page ,
        'paged' => $paged,
        'cat' => $cat
    );
elseif(is_search()):
    $s = $_GET['s'];
    $args = array(
        'post_type'=> 'post',
        'posts_per_page'=> $post_per_page ,
        'paged' => $paged ,
        's' => $s
    );
elseif(is_author()):
    global $wp_query;
    $curauth = $wp_query->get_queried_object();
    $args = array(
        'post_type'=> 'post',
        'posts_per_page'=> $post_per_page ,
        'paged' => $paged,
        'author' => $curauth->ID
    );
    
else:
    $args = array(
        'post_type'=> 'post',
        'posts_per_page'=> $post_per_page ,
        'paged' => $paged
    );
endif;
$loop = new WP_Query($args);
?>

<div class="w-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <?php
                        if($loop->have_posts()):
                            while($loop->have_posts()) : $loop->the_post();
                                get_box_blog();
                            endwhile;
                        else:
                            echo '<div class="col-md-12 col-xs-12">';
                                echo '<article class="post-holder">';
                                    echo '<div class="post-block mb40">';
                                        echo '<h2>Nenhum resultado encontrado.</h2>';
                                    echo '</div>';
                                echo '</article>';
                            echo '</div>';
                        endif;

                        
                        if(function_exists('wp_pagenavi')) { 
                            echo '<div class="col-md-12 text-center col-xs-12">';
                                echo '<div class="st-pagination">';
                                    wp_pagenavi(array( 'query' => $loop )); 
                                echo '</div>';
                            echo '</div>';
                        }
                        ?>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <?php get_sidebar(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>