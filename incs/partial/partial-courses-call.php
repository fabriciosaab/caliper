<div id="video" class="section-space80">
    <div class="container">
        <div class="row">
            <div class="offset-md-2 col-md-8 col-sm-12 col-xs-12">
                <?php
                $title_to_action_home = get_field('title_to_action_home','options');
                if($title_to_action_home):
                ?>
                    <div class="text-center section-title mb30">
                        <h2><?php echo $title_to_action_home; ?></h2>
                    </div>
                <?php
                endif;
                $video = get_field('url_youtube_call_to_action','options');
                parse_str( parse_url( $video, PHP_URL_QUERY ), $parameter );
                ?>
                <div class="embed-responsive mb30">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $parameter['v']?>"></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            $title_button_call_to_action = get_field('title_button_call_to_action','options');
            if($title_button_call_to_action):
                $link_button_call_to_action = get_field('link_button_call_to_action','options');
            ?>
                <div class="offset-md-3 col-md-6"> <a href="<?php echo ($link_button_call_to_action == true ? $link_button_call_to_action : '#'); ?>" class="btn btn-default btn-lg"><?php echo $title_button_call_to_action; ?></a> </div>
            <?php
            endif;
            ?>
        </div>
    </div>
</div>