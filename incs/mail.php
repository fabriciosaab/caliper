<?php
// MAIL
// -- IMPORT PHPMAILER
require(TEMPLATEPATH . "/incs/extension/phpmailer/PHPMailerAutoload.php");
// -- FUNCTION
function psn_email($title, $html, $sendTo,$returnStatus = true){	
    global $template_caliper;

	$wrapperHtml = $html;	
	$mail = new PHPMailer();		
	
    $mail->IsSMTP();
	$mail->Port = 465; 
	$mail->Host = 'smtp.uni5.net';
	$mail->SMTPSecure = "ssl";
	$mail->SMTPAuth = true; 
    $mail->Username = 'comercial@escolacaliper.com.br'; 
    $mail->Password = "liper@1620"; 

    $mail->FromName = "Site - " . $template_caliper['name'];
	$mail->From = "comercial@escolacaliper.com.br"; 
    $mail->Sender = "comercial@escolacaliper.com.br";	

	$mail->AddAddress($sendTo);	
	$mail->IsHTML(true); 	
	$mail->CharSet = 'utf-8'; 	
	$mail->Subject = $title;	
	$mail->Body = $wrapperHtml;	
	$enviado = $mail->Send();	
	$mail->ClearAllRecipients();	
	$mail->ClearAttachments();

	if($returnStatus):
		$status = array(); 
		if($mail)
			$status =  array( 'cod' => 1 , 'message' =>  'Enviado com sucesso' );
		else
			$status =  array( 'cod' => 2 , 'message' =>  'Não enviado' );

		$return = json_encode($status);

		if(is_array($return)) {
			print_r($return); 
		} else {
			echo $return;
		} 
		die;
	  	return false;
	endif;
}
// -- AJAX SEND MAIL
add_action('wp_ajax_nopriv_sendContact', 'sendContact');  
add_action('wp_ajax_sendContact', 'sendContact');
function sendContact(){ 	
	global $wpdb;
    global $template_caliper;
	$valid = check_ajax_referer('form_contact', 'verify');
	if(isset($_POST) && $valid ) {
		$sendTo = $_POST['sendTo'];
		$name = $_POST['nameContact'];
		$email = $_POST['emailContact'];
		$subject = $_POST['subjectContact'];
		$phone = $_POST['phoneContact'];
		$message = $_POST['messageContact'];
		$html = '';
		$html .= '<h2>Contato do Site - '. $template_caliper["name"] .'</h2>';
		$html .= '<p><strong>Assunto:</strong> '. $subject .'</p>';
		$html .= '<p><strong>Nome:</strong> '. $name .'</p>';
		$html .= '<p><strong>E-mail:</strong> '. $email .'</p>';
		$html .= '<p><strong>Telefone:</strong> '. $phone .'</p>';
		$html .= '<p><strong>Mensagem:</strong> '. $message .'</p>';		
		$title = 'Contato Site';		

		psn_email($title, $html, $sendTo);
	}
}
?>