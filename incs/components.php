<?php
function get_course_box($link='',$category_box='',$load_time='',$image='',$title=''){
    $separator = ' ';
    $output = '';
    if(!empty($category_box)) :
        foreach($category_box as $cat):
            $output .= $cat->slug . $separator;
            $color = get_field("color_button", $cat);
        endforeach;
    endif;
    $class = '';
    if(!empty($category_box)) { 
        $class .= trim($output,$separator);
    };
    if($color) {
        $class .= ' curso'.$color;
    }
    $return = '';
    $return .= '<div class="curso '. $class .' col-md-4 col-sm-6">';
        $return .= '<div class="cursoIn">';
            if($image):
                $return .= '<a href="'. $link .'" class="imghover">';
                    $return .= $image;
                $return .= '</a>';
            endif;
            $return .= '<div class="hCurso">';
                $return .= '<h3 class="hCursoIn"><a href="'. $link .'">'. $title .'</a></h3>';
            $return .= '</div>';

            $return .= '<div class="row">';
                $time = ($load_time == true ? "Carga Horária: " . $load_time : '' );
                $return .= '<div class="col-md-7 col-sm-7 col-xs-6">' . $time . '</div>';
                $return .= '<div class="col-md-5 col-sm-5 col-xs-6 text-end"><a class="btn" href="'. $link .'">Saiba mais</a></div>';
            $return .= '</div>';

        $return .= '</div>';
    $return .= '</div>';

    return $return;
}

function get_lead_content_box(){
    $postID = get_the_ID();
    $image = get_the_post_thumbnail( $postID , '426x284', array( 'class' => 'img-responsive' ) );
    $link = '';
    $title = get_the_title();
    $archive = get_field('archive');
    $archive = $archive['url'];
?>
    <div class="col-md-6 col-sm-12 col-xs-12 mb60">
        <div class="row">
            <?php
            if($image):
                echo '<div class="col">';
                    echo '<a  href="'. $archive .'" target="_blank" class="imghover">';
                        echo $image;
                    echo '</a>';
                echo '</div>';
            endif;
            ?>
            <div class="col position-relative">
                <p class="conteudo-name"><?php echo $title;?></p>
                <span class="conteudo-excerpt"><?php the_excerpt(); ?></span>
                <a class="btn btn-sm btn-danger" href="<?php echo $archive?>" target="_blank"
                style="border-radius: 13px; padding: 10px 30px; position: absolute; bottom: 0;" >
                    DOWNLOAD
                </a>
            </div>
        </div>
    </div>
<?php
}
function get_box_blog(){
    $author = get_the_author_meta('display_name');

    $title = get_the_title();
    $link = get_permalink();
    $url_youtube = get_field('url_youtube');
    $image = get_the_post_thumbnail( $post->ID , '750x410', array( 'class' => 'img-responsive' ) );
?>
    <div class="col-md-12 col-xs-12">
        <article class="post-holder">
            <div class="post-block mb40">
            <div class="post-img">
                <?php
                if($url_youtube):
                    parse_str( parse_url( $url_youtube, PHP_URL_QUERY ), $parameter );
                ?>
                    <div class="embed-responsive">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php echo $parameter['v']; ?>"></iframe>
                    </div>	
                <?php
                else:
                ?>
                    <a href="<?php echo $link; ?>" class="imghover"><?php echo $image; ?></a>
                <?php
                endif;
                ?>
            </div>
            <div class="bg-white">
                <h1><a href="<?php echo $link; ?>" class="title"><?php the_title(); ?></a></h1>
                <p class="meta"> <span class="meta-date"><?php the_time('d \d\e F \d\e Y')?></span> <span class="meta-author">Autor:<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="meta-link"> <?php echo $author; ?></a></span> </p>
                <?php
                    the_excerpt();
                ?>
                <a href="<?php echo $link; ?>" class="btn-link">Saiba mais</a> </div>
            </div>
        </article>
    </div>
<?php
}

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');


function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

?>