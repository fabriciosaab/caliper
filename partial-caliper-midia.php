<?php
$category_midia = get_field('category_midia','options');
$args = array(
    'post_type'=> 'post',
    'posts_per_page'=> 3,
    'category_name' => $category_midia->slug
);
$loop = new WP_Query($args);
if ($loop->have_posts()) :
?>

    <div id="midia" class="section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12">
                    <div class="mb60 text-center section-title">
                        <h2>Caliper na mídia</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php
                while($loop->have_posts()) : $loop->the_post();
                    $title = get_the_title();
                    $link = get_permalink();
                    $video = get_field('url_youtube');
                    $image = get_the_post_thumbnail( $post->ID , '554x256_NO', array( 'class' => 'img-responsive mb10' ) );
                    if($video):
                        parse_str( parse_url( $video, PHP_URL_QUERY ), $parameter );
                        if(!$image)
                            $image = '<img src="https://i1.ytimg.com/vi/'. $parameter["v"] .'/0.jpg" alt="'. $title .'" class="img-responsive mb10">';
                    endif;
                ?>
                    <div class="col-md-4 col-sm-4 col-xs-12 mb60"> 
                        <a href="<?php echo $link; ?>" class="imgVideo">
                            <?php echo ($image != false ? $image : '' ); ?>
                        </a>
                        <a href="<?php echo $link; ?>"><?php echo $title;?></a>
                    </div>
                <?php
                endwhile;
                ?>
            </div>
            <div class="row">
                <div class="col-md-offset-4 col-md-4"> <a href="<?php echo get_category_link($category_midia->term_id);?>" class="btn btn-success btn-lg btn-block">VER MAIS</a> </div>
            </div>
        </div>
    </div>
<?php
endif;
?>
