<?php
get_header();
    if (have_posts()) :
        while(have_posts()) : the_post();
            $gallery = get_field('gallery');
            get_template_part( 'incs/partial/partial', 'box-title' );
?>
            <!-- content start -->
            <div class="bg-white">
                <div class="container pt-5">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="section-about">
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php //the_content(); ?>
                                            <p>&nbsp;</p>
                                        </div>
                                    </div>
                                    <div class="row mb60">
                                        <div class="col-sm-12 col-md-8 offset-md-2">
                                            <form class="contact-us row" id="contact-form">
                                                <input type="hidden" name="action" value="sendContact">
                                                <input type="hidden" name="sendTo" value="<?php the_field('email')?>">
                                                <?php wp_nonce_field('form_contact', 'verify'); ?>
                                                <!-- Text input-->
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="nameContact">Nome</label>
                                                        <input id="nameContact" name="nameContact" type="text" placeholder="Nome" class="form-control cont input-md" required="required">
                                                    </div>
                                                </div>
                                                <!-- Text input-->
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="emailContact">E-mail</label>
                                                        <input id="emailContact" name="emailContact" type="email" placeholder="E-mail" class="form-control cont input-md" required="required">
                                                    </div>
                                                </div>
                                                <!-- Text input-->
                                                
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="phoneContact">Telefone</label>
                                                        <input id="phoneContact" name="phoneContact" type="tel" placeholder="Telefone" class="form-control cont input-md" required="">
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label class="sr-only control-label" for="subjectContact">Assunto</label>
                                                        <input id="subjectContact" name="subjectContact" type="text" placeholder="Assunto" class="form-control cont input-md" required="required">
                                                    </div>
                                                </div>
                                                <!-- Select Basic -->
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label class="control-label" for="messageContact"> </label>
                                                        <textarea class="form-control cont" id="messageContact" rows="7" required="required" name="messageContact" placeholder="Mensagem"></textarea>
                                                    </div>
                                                </div>
                                                <!-- Button -->
                                                <div class="col-md-12">
                                                    <input type="submit" class="btn btn-danger" value="Enviar" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php
    endwhile;
    endif;
    get_footer();
?>