<?php
//Configurantion Global
global $template_caliper;
$template_caliper = array(
    'template_name' => 'caliper',
    'name' => 'Caliper - Clínica de Imagem',
    'url_site' => get_bloginfo("url"),
    'url_template' => get_template_directory_uri()
);

// Constant URL TEMPLATE
define('URL_TEMPLATE', $template_caliper['url_template']);
// Constant URL SITE
define('URL_SITE', $template_caliper['url_site']);

// Functions Template
require_once('incs/functions-template.php');

// PostType
require_once('incs/post-type/teams.php');
require_once('incs/post-type/interns.php');
require_once('incs/post-type/courses.php');
require_once('incs/post-type/banners.php');
require_once('incs/post-type/testimonials.php');
require_once('incs/post-type/lead_content.php');
require_once('incs/post-type/exams.php');
require_once('incs/post-type/units.php');
?>