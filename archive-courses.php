<?php
get_header();
    get_template_part('incs/partial/partial','banners');
    get_template_part('incs/partial/partial','courses');

add_footer('css_banner');
function css_banner(){
    echo '<link href="'. URL_TEMPLATE .'/css/owl.carousel.css" rel="stylesheet">';
    echo '<link href="'. URL_TEMPLATE .'/css/owl.theme.css" rel="stylesheet">';
    echo '<script type="text/javascript" src="'. URL_TEMPLATE .'/js/owl.carousel.min.js"></script>';
}
get_footer();